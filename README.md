lint
====

A little linter written in `bash`. It checks for trailing whitespace, tabs,
no trailing blank line, multiple trailing blank lines, and underscores in
file names. You can modify the script to add your own rules.

Checkout the example at the bottom to see how you can integrate `lint` with
`git` so your addded or modified files are linted whenever you do a `git add`.

Installation
------------

Install `lint` into `/usr/local/bin`.

```Shell
git clone https://gitlab.com/narvin/lint.git
cd lint
sudo make
```

Uninstall `lint`.

```Shell
sudo make uninstall
```

Usage
-----

### Standalone

Lint 3 files.

```Shell
lint foo bar baz
```

Lint files in the `src` directory.

```Shell
lint -d=src foo bar baz
```

### With git

Lint all the files in the current directory, excluding any .git directories.

```Shell
find . -type d -name .git -prune -o -type f -print0 | xargs -0 lint
```

Upgrade `git add` to `git al` (git add and lint) by putting this alias in
your git config to add files, then lint all files that have been added,
modified, or renamed in the index.

```Ini
[alias]
  al = !git add "$@" && \
    git status -z --porcelain | grep -z '^[AMR] ' | cut -z -b 4- \
    | xargs -0 lint -d "$(git rev-parse --show-toplevel)" && :
```

