all: install

install:
	cp ./lint /usr/local/bin

uninstall:
	$(RM) /usr/local/bin/lint

test:
	./lint-test-init
	lint ./lint-test/*

clean:
	rm -rf lint-test

